from scipy.spatial import distance
from scipy.stats import pearsonr
import numpy as np
import os.path
import glob
import cv2
import os



def check_img_amount_in_folder(path):
    list = os.listdir(path)

    images_list = []
    for idx in range(len(list)):
        if list[idx].find(".bmp") != -1 or list[idx].find(".png") != -1 or list[idx].find(".jpg") != -1:
            images_list.append(list[idx])
    return len(images_list)


def load_images_from_folder(folder):
    images = []
    #for filename in os.listdir(folder)[1:]:
    for filename in os.listdir(folder)[:]:
        img = cv2.imread(os.path.join(folder, filename))
        if img is not None:
            images.append(img)
    return images


def load_last_image(path):
    # todo check which file

    file_type = '\*bmp'
    files = glob.glob(path + file_type)
    #max_file = max(files, key=os.path.getctime)  # old
    # max_file = sorted(files, key=os.path.getmtime)[-1] # old
    max_file = sorted(files, key=os.path.getmtime)[0]
    max_file_name = max(files, key=os.path.getctime).split('\\')[-1]

    last_image = cv2.imread(max_file, cv2.IMREAD_GRAYSCALE)
    #print(max_file)

    return last_image, max_file_name


# def matchTemplate:
#     template_path = r'C:\Users\or.zilberberg\Desktop\Or\Daimler\data\manual_station\new_data\valid\pattern\pattern.bmp'
#     template = cv2.imread(template_path, 0)
#     w, h = template.shape[::-1]
#
#     res = cv2.matchTemplate(average_gray, template,cv2.TM_CCOEFF_NORMED)
#     threshold = 0.8
#     loc = np.where( res >= threshold)
#     for pt in zip(*loc[::-1]):
#         cv2.rectangle(average, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)


def crop_rect(img, rect):
    # get the parameter of the small rectangle
    center = rect[0]
    size = rect[1]
    angle = rect[2]
    center, size = tuple(map(int, center)), tuple(map(int, size))

    # get row and col num in img
    rows, cols = img.shape[0], img.shape[1]

    M = cv2.getRotationMatrix2D(center, angle, 1)
    img_rot = cv2.warpAffine(img, M, (cols, rows))
    out = cv2.getRectSubPix(img_rot, size, center)

    return out, img_rot


def crop_tags(tags, imgt):
    golden_tags_crop_arr = []
    for tag in range(len(tags)):
        x1 = round(tags[tag].corners[2][0])
        y1 = round(tags[tag].corners[2][1])
        x2 = round(tags[tag].corners[1][0])
        y2 = round(tags[tag].corners[1][1])
        x3 = round(tags[tag].corners[0][0])
        y3 = round(tags[tag].corners[0][1])
        x4 = round(tags[tag].corners[3][0])
        y4 = round(tags[tag].corners[3][1])

        cnt = np.array([[[x1, y1]],
                        [[x2, y2]],
                        [[x3, y3]],
                        [[x4, y4]]
                        ])

        rect = cv2.minAreaRect(cnt)
        # print("rect: {}".format(rect))

        # crop the rotated rectangle from the image
        im_crop, img_rot = crop_rect(imgt, rect)
        golden_tags_crop_arr.append(im_crop)

    return golden_tags_crop_arr


def illumination_feature_extraction(tags_crop_arr):
    # function get april tags array:
    # feature is histogram - 1d array
    tags_ill_features_arr = []
    for index in range(len(tags_crop_arr)):

        histogram, bin_edges = np.histogram(tags_crop_arr[index], bins=256)
        tags_ill_features_arr.append(histogram)

    print("April tag illumination feature extract done")
    return tags_ill_features_arr


def focus_feature_extraction(tags_crop_arr):
    # function get april tags array:
    # feature is variance of laplacian - 1d array
    tags_focus_features_arr = []
    for idx in range(len(tags_crop_arr)):

        tag_focus_score = cv2.Laplacian(tags_crop_arr[idx], cv2.CV_64F).var()
        tags_focus_features_arr.append(tag_focus_score)

    print("April tag focus feature extract done")
    return tags_focus_features_arr


def pos_feature_extraction(tags_arr):
    # function get tags and return array of points
    # feature is coordinate of tag

    tags_pos_features_arr = []

    for tag_idx in range (len(tags_arr)):
        X1 = (tags_arr[tag_idx].corners[2][0])
        Y1 = (tags_arr[tag_idx].corners[2][1])
        X2 = (tags_arr[tag_idx].corners[0][0])
        Y2 = (tags_arr[tag_idx].corners[0][1])

        tag_x_center = ((X1 + X2) / 2)
        tag_y_center = ((Y1 + Y2) / 2)
        tags_pos_features_arr.append((tag_x_center, tag_y_center))

    print("April tag pos feature extract done")
    return tags_pos_features_arr


def illumination_score(test_tags_ill_features_arr, golden_tags_ill_features_arr):
    # function get 2 arrays of histograms
    ill_score_arr = []
    for index in range(len(test_tags_ill_features_arr)):

        # correlation
        corr, _ = pearsonr(test_tags_ill_features_arr[index], golden_tags_ill_features_arr[index])
        ill_score_arr.append(corr)
    print("April tag illumination score: {}".format(ill_score_arr))

    return ill_score_arr


def focus_score(test_tags_focus_features_arr, golden_tags_focus_features_arr):
    # function get 2 arrays of Laplacian
    focus_score_arr = []
    for idx in range(len(test_tags_focus_features_arr)):

        comparison_score = test_tags_focus_features_arr[idx]/golden_tags_focus_features_arr[idx]
        focus_score_arr.append(comparison_score)
    print("April tag focus scores: {}".format(focus_score_arr))

    return focus_score_arr


def positioning_score(test_tags_pos_features_arr, golden_tags_pos_features_arr):
    pos_score_arr = []
    for idx in range(len(test_tags_pos_features_arr)):
        pos_score_arr.append(distance.euclidean(golden_tags_pos_features_arr[idx], test_tags_pos_features_arr[idx]))
    print("April tag pos scores: {}".format(pos_score_arr))

    return pos_score_arr


def new_illumintaion_score(golden_tags_crop_arr, test_tags_crop_arr):
    mse_ill_score_arr = []
    for idx in range(len(test_tags_crop_arr)):
        mse_ill_score_arr.append(mse(golden_tags_crop_arr[idx], test_tags_crop_arr[idx]))
    print("April tag new illumination score: {}".format(mse_ill_score_arr))

    return mse_ill_score_arr


def mse(imageA, imageB):
    # the 'Mean Squared Error' between the two images is the
    # sum of the squared difference between the two images;
    # NOTE: the two images must have the same dimension
    # resize
    if imageA.shape[1] != imageB.shape[1] or imageA.shape[0] != imageB.shape[0]:
        dim = (imageA.shape[1], imageA.shape[0])
        imageB = cv2.resize(imageB, dim, interpolation=cv2.INTER_AREA)

    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    invert_im = 255 -((imageA.astype("float") - imageB.astype("float")) ** 2)
    sum_invert_mse_im = np.sum(invert_im)
    score = sum_invert_mse_im/((imageA.shape[0] * imageA.shape[1]) * 255)
    score_2 = np.sum((255 - (imageA.astype("float") - imageB.astype("float"))) ** 2) / (
                (imageA.shape[0] * imageA.shape[1]) * 255 ** 2)
    return score_2


def save_goledn_image(last_images_path, average_gray):
    golden_path = last_images_path + "\\" + "golden" + "\\" + 'golden_im.bmp'

    exist_folder = os.path.exists(last_images_path + "\\" + "golden")
    if exist_folder:
        cv2.imwrite(golden_path, average_gray)
    else:
        golden_path_folder = last_images_path + "\\" + "golden"
        os.mkdir(golden_path_folder)
        cv2.imwrite(golden_path, average_gray)
    print("golden image saved")


def annotate_golden_image(tags, image):
    # function add annotaion on image acoording to detected tags
    for tag_idx in range(len(tags)):
        X1 = round(tags[tag_idx].corners[2][0])
        Y1 = round(tags[tag_idx].corners[2][1])
        X2 = round(tags[tag_idx].corners[0][0])
        Y2 = round(tags[tag_idx].corners[0][1])

        tag_x_center = ((X1 + X2) / 2)
        tag_y_center = ((Y1 + Y2) / 2)

        font = cv2.FONT_HERSHEY_SIMPLEX
        bottomLeftCornerOfText = (round(tag_x_center), round(tag_y_center))
        fontScale = 1
        fontColor = (255, 0, 0)
        lineType = 2
        cv2.putText(image, str(tag_idx),
                    bottomLeftCornerOfText,
                    font,
                    fontScale,
                    fontColor,
                    lineType)

        for i in range(0, 4):
            j = (i + 1) % 4
            cv2.line(image,
                     (round(tags[tag_idx].corners[i][0]), round(tags[tag_idx].corners[i][1])),
                     (round(tags[tag_idx].corners[j][0]), round(tags[tag_idx].corners[j][1])),
                     (255, 0, 0), 10)
    return image


def save_tags_coordinate(path, tags_coordintat):
    txt_file_path = path + "\\" + "tags_coordinate" + "\\" + 'tags_coordinate.txt'

    exist_folder = os.path.exists(path + "\\" + "tags_coordinate")
    if exist_folder:
        f = open(txt_file_path, "a")
        f.write(str(tags_coordintat))
        f.close()
    else:
        coordinate_path_folder = path + "\\" + "tags_coordinate"
        os.mkdir(coordinate_path_folder)
        f = open(txt_file_path, "a")
        f.write(str(tags_coordintat))
        f.close()
    print("tags coordinate saved")