from pupil_apriltags import Detector
from time import gmtime, strftime
import matplotlib.pyplot as plt
from scipy import ndimage
import numpy as np
import Functions
import getpass
import errno
import time
import csv
import cv2
import os

flag_open_csv = 0
flag_golden_created = 0

## tag detector configuration
at_detector = Detector(families='tag36h11',
                       nthreads=1,
                       quad_decimate=1.0,
                       quad_sigma=0.0,
                       refine_edges=1,
                       decode_sharpening=0.25,
                       debug=0)
fx = fy = 6
tag_size = 47 / 1000

## check how many images in the folder
user = getpass.getuser()
# last_images_path = r'C:\Users' + "\\" + user + r'\Documents\Matrox Design Assistant\SavedImages\ST67\Back99x\PassImages'

# last_images_path = r'C:\Users\or.zilberberg\Desktop\Or\vision_self_diagnostic\april_tags_deignostic\focus\last_images'
# last_images_path = r'C:\Users\or.zilberberg\OneDrive - Bright Machines\back_72x\FailImages\last_im'
last_images_path = r'C:\Users\or.zilberberg\Desktop\Or\vision_self_diagnostic\mse_score_test\250m'


while 1:
    try:
        images_amount = Functions.check_img_amount_in_folder(last_images_path)

        if images_amount < 0:
            print('waiting for golden images...')
            time.sleep(10)
        else:
            if flag_golden_created == 0:
                # load images
                numpy_images = Functions.load_images_from_folder(last_images_path)
                # create golden
                # average image:
                average = np.average(numpy_images, axis=0).astype(np.uint8)
                average_gray = cv2.cvtColor(average, cv2.COLOR_BGR2GRAY)

                # plt.figure(figsize=(16, 5))
                # plt.subplot(121)
                # plt.imshow(average_gray,cmap='gray', interpolation='nearest')
                # plt.title('Golden image', fontsize=20)
                # plt.subplot(122)
                # plt.imshow(average_gray_med,cmap='gray', interpolation='nearest')
                # plt.title('Median filter', fontsize=20)

                # std image:
                # std = np.std(numpy_images, axis=0).astype(np.uint8)
                # variance image :
                # var = np.var(numpy_images, axis=0)

                # detect tags from golden:
                cx, cy = average_gray.shape[0] / 2, average_gray.shape[1] / 2
                golden_tags = at_detector.detect(average_gray, estimate_tag_pose=True, camera_params=[fx, fy, cx, cy],
                                                 tag_size=tag_size)

                # annotate on golden im
                annotate_golden_image = Functions.annotate_golden_image(golden_tags, average)

                # save golden im
                Functions.save_goledn_image(last_images_path, annotate_golden_image)
                # Functions.save_goledn_image(last_images_path, average_gray)

                if len(golden_tags) == 0:
                    pass
                else:
                    golden_tags_crop_arr = Functions.crop_tags(golden_tags, average_gray)

                    # feature extractions from golden tags:
                    # Illumination
                    golden_tags_ill_features_arr = Functions.illumination_feature_extraction(golden_tags_crop_arr)
                    # Focus
                    golden_tags_focus_features_arr = Functions.focus_feature_extraction(golden_tags_crop_arr)
                    # Position
                    golden_tags_pos_features_arr = Functions.pos_feature_extraction(golden_tags)
                    # save coordinate in text
                    Functions.save_tags_coordinate(last_images_path, golden_tags_pos_features_arr)

                    flag_golden_created = 1

            last_test_image, last_file_name = Functions.load_last_image(last_images_path)

            # check if there is new image
            while 1:
                # new_images_amount = Functions.check_img_amount_in_folder(last_images_path)
                new_last_test_image, new_last_file_name = Functions.load_last_image(last_images_path)
                err = np.sum((last_test_image.astype("float") - new_last_test_image.astype("float")) ** 2)
                if last_file_name != new_last_file_name or err > 0:
                    last_file_name = new_last_file_name
                    break
                else:
                    time.sleep(1)
                    print('Wait for test image...')

            last_test_image, b = Functions.load_last_image(last_images_path)
            # last_test_image_med = ndimage.median_filter(ndimage.median_filter(last_test_image, 3), 3)
            last_test_image_med = ndimage.median_filter(last_test_image, 3)
            last_test_image_tags = at_detector.detect(last_test_image, estimate_tag_pose=True,
                                                      camera_params=[fx, fy, cx, cy],
                                                      tag_size=tag_size)

            if len(last_test_image_tags) != len(golden_tags):
                continue

            # print("finish detected tag")
            test_tags_crop_arr = Functions.crop_tags(last_test_image_tags, last_test_image_med)

            # feature extractions from test tags:
            # Illumination
            test_tags_ill_features_arr = Functions.illumination_feature_extraction(test_tags_crop_arr)
            # Focus
            test_tags_focus_features_arr = Functions.focus_feature_extraction(test_tags_crop_arr)
            # Position
            test_tags_pos_features_arr = Functions.pos_feature_extraction(last_test_image_tags)

            # print("finish feature extract from test im")

            # Give scores
            # Illumination score arr
            #ill_score_arr = Functions.illumination_score(test_tags_ill_features_arr, golden_tags_ill_features_arr)
            # Focus score arr
            focus_score_arr = Functions.focus_score(test_tags_focus_features_arr, golden_tags_focus_features_arr)
            # Position score arr
            pos_score_arr = Functions.positioning_score(test_tags_pos_features_arr, golden_tags_pos_features_arr)


            # def mse(imageA, imageB):
            #     # the 'Mean Squared Error' between the two images is the
            #     # sum of the squared difference between the two images;
            #     # NOTE: the two images must have the same dimension
            #     err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
            #     norm_err = float(imageA.shape[0] * imageA.shape[1]) / err
            #     return norm_err
            #
            # mes_score = mse(average_gray, last_test_image)
            # print("finish give scores")

            mse_ill_score_arr = Functions.new_illumintaion_score(golden_tags_crop_arr, test_tags_crop_arr)

            # Extract results
            header = ['Date_Time', 'Tag1_Focus_score', 'Tag2_Focus_score', 'Tag3_Focus_score', 'Tag1_Pos_score',
                      'Tag2_Pos_score', 'Tag3_Pos_score', 'Tag1_light_score', 'Tag2_light_score', 'Tag3_light_score']
            date_time = [strftime("%Y-%d-%m %H:%M:%S", gmtime())]
            data = date_time + focus_score_arr + pos_score_arr + mse_ill_score_arr
            path_file = last_images_path + '\\diagnostic_features\\Scores.csv'

            # filename = last_images_path + '\\diagnostic_features\\focus_Features.png'
            filename = path_file

            if not os.path.exists(os.path.dirname(filename)):
                try:
                    os.makedirs(os.path.dirname(filename))
                except OSError as exc:  # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise

            if flag_open_csv == 0:
                # write header
                with open(path_file, 'w', encoding='UTF8', newline='') as f:
                    writer = csv.writer(f)
                    # write the header
                    writer.writerow(header)
                flag_open_csv = 1

            with open(path_file, 'a', encoding='UTF8', newline='') as f:
                writer = csv.writer(f)
                # write the data
                writer.writerow(data)

            ## graph and reports
            # Create a new figure, plot into it, then close it so it never gets displayed
            # Focus
            # plt.figure()
            # plt.scatter([*range(0, len(golden_tags_focus_features_arr))], golden_tags_focus_features_arr, color='orange',
            #            label='Golden')
            # plt.scatter([*range(0, len(test_tags_focus_features_arr))], test_tags_focus_features_arr, color='blue',
            #            label='Test')
            # plt.title('Focus Score')
            # plt.xlabel('Tags ID')
            # plt.ylabel('Laplacian val')
            # plt.legend()

        # Illumination
        # plt.figure()
        # plt.scatter([*range(0, len(golden_tags_ill_features_arr))], golde, color='orange',
        #             label='Golden')
        # plt.scatter([*range(0, len(test_tags_ill_features_arr))], test_tags_focus_features_arr, color='blue',
        #             label='Test')
        # plt.legend()

        # path_file = last_images_path + '\\diagnostic_features\\focus_Features.png'
        # plt.savefig(path_file)

    except:
        print("An exception occurred")

        # for graph_idx in range(0, len(golden_tags_ill_features_arr)):
        #     filename = last_images_path
        #
        #     fig = plt.plot(golden_tags_ill_features_arr[graph_idx])
        #     plt.savefig(filename)

#
# ## ilumintaion score
# illu_golden = cv2.imread(r"C:\Users\or.zilberberg\Desktop\Or\vision_self_diagnostic\april_tags_deignostic\illumintaion\50m.jpg", cv2.IMREAD_GRAYSCALE)
# illu_test = cv2.imread(r"C:\Users\or.zilberberg\Desktop\Or\vision_self_diagnostic\april_tags_deignostic\illumintaion\80m.jpg", cv2.IMREAD_GRAYSCALE)
# # illu_test = cv2.imread(r"C:\Users\or.zilberberg\Desktop\Or\vision_self_diagnostic\april_tags_deignostic\illumintaion\50m_plus_strong_top_light.jpg",cv2.IMREAD_GRAYSCALE)
#
#
# # golden
# golden_tags = at_detector.detect(illu_golden, estimate_tag_pose=True, camera_params=[fx, fy, cx, cy], tag_size=tag_size)
# tags_num = len(golden_tags)
#
# golden_tags_crop_arr = crop_tags(golden_tags, illu_golden)
#
# # test
# test_tags = at_detector.detect(illu_test, estimate_tag_pose=True, camera_params=[fx, fy, cx, cy], tag_size=tag_size)
# test_tags_crop_arr = crop_tags(test_tags, illu_test)
#
#
# def illumintaion_score(golden_tags_crop_arr, test_tags_crop_arr):
#     # function get 2 crop april tags arrays:
#     # 1. golden array
#     # 2. test array
#     ill_score_arr = []
#     for index in range(len(test_tags_crop_arr)):
#
#         histogram_golden, bin_edges = np.histogram(golden_tags_crop_arr[index], bins=256)
#         histogram_test, bin_edges = np.histogram(test_tags_crop_arr[index], bins=256)
#
#         plt.figure()
#         plot_lines = []
#         # plt.hist(histogram_golden, list(range(0, 255)))
#         # plt.hist(histogram_test, list(range(0, 255)))
#         # plt.plot(histogram_golden)
#         # plt.plot(histogram_test)
#
#         l1, = plt.plot(histogram_golden, '-')
#         l2, = plt.plot(histogram_test, '--')
#         plot_lines.append([l1, l2])
#         plt.legend(plot_lines[0], ["20m", "80m"], loc=1)
#         plt.show()
#
#         # correlation
#         corr, _ = pearsonr(histogram_test, histogram_golden)
#         ill_score_arr.append(corr)
#         print("print april tag illumination score: {}".format(corr))
#
#     return ill_score_arr
#
# ill_score_arr = illumintaion_score(golden_tags_crop_arr, test_tags_crop_arr)
#
# # plt.figure()
# # plt.plot(histogram_golden)
# # plt.plot(histogram_test)
# # base_base = cv2.compareHist(histogram_test, histogram_golden, cv2.HISTCMP_CORREL)
#
# ## focus score
# focus_golden = cv2.imread(r"C:\Users\or.zilberberg\Desktop\Or\vision_self_diagnostic\april_tags_deignostic\focus\50m_focus.jpg",cv2.IMREAD_GRAYSCALE)
# focus_test = cv2.imread(r"C:\Users\or.zilberberg\Desktop\Or\vision_self_diagnostic\april_tags_deignostic\focus\50m_focus_1.jpg",cv2.IMREAD_GRAYSCALE)
#
# tags = at_detector.detect(focus_golden, estimate_tag_pose=True, camera_params=[fx, fy, cx, cy], tag_size=tag_size)
# tags_test = at_detector.detect(focus_test, estimate_tag_pose=True, camera_params=[fx, fy, cx, cy], tag_size=tag_size)
#
# golden_tags_crop_arr = crop_tags(tags, focus_golden)
# test_tags_crop_arr = crop_tags(tags_test, focus_test)
#
# def focus_score(golden_tags_crop_arr, test_tags_crop_arr):
#     focus_score_arr = []
#     for idx in range(len(test_tags_crop_arr)):
#
#         golden_tag_idx_score = cv2.Laplacian(golden_tags_crop_arr[idx], cv2.CV_64F).var()
#         test_tag_idx_score = cv2.Laplacian(test_tags_crop_arr[idx], cv2.CV_64F).var()
#
#         print("golden tag {} focus score: {}".format(idx, cv2.Laplacian(golden_tags_crop_arr[idx], cv2.CV_64F).var()))
#         print("test tag {} focus score: {}".format(idx, cv2.Laplacian(test_tags_crop_arr[idx], cv2.CV_64F).var()))
#         comparison_score = test_tag_idx_score/golden_tag_idx_score
#         focus_score_arr.append(comparison_score)
#     return focus_score_arr
#
# focus_score_arr = focus_score(golden_tags_crop_arr, test_tags_crop_arr)
#
# ## positioning score
# def get_pos(tags):
#     # function get tags and return array of points
#     points_arr = []
#     for tag_idx in range (len(tags)):
#         X1 = round(tags[tag_idx].corners[2][0])
#         Y1 = round(tags[tag_idx].corners[2][1])
#         X2 = round(tags[tag_idx].corners[0][0])
#         Y2 = round(tags[tag_idx].corners[0][1])
#
#         tag_x_center = round((X1 + X2) / 2)
#         tag_y_center = round((Y1 + Y2) / 2)
#         points_arr.append((tag_x_center, tag_y_center))
#
#     return points_arr
#
# pos_golden = cv2.imread(r"C:\Users\or.zilberberg\Desktop\Or\vision_self_diagnostic\april_tags_deignostic\pos\50m_pos.jpg",cv2.IMREAD_GRAYSCALE)
# pos_test = cv2.imread(r"C:\Users\or.zilberberg\Desktop\Or\vision_self_diagnostic\april_tags_deignostic\pos\50m_pos_1.jpg",cv2.IMREAD_GRAYSCALE)
#
# tags = at_detector.detect(pos_golden, estimate_tag_pose=True, camera_params=[fx, fy, cx, cy], tag_size=tag_size)
# tags_test = at_detector.detect(pos_test, estimate_tag_pose=True, camera_params=[fx, fy, cx, cy], tag_size=tag_size)
#
# points_arr_golden = get_pos(tags)
# points_arr_test = get_pos(tags_test)
#
# # print("golden tag pos score: {}".format(abs(points_arr_golden[1][0] - points_arr_test[1][0])/pos_golden.shape[0]))
# # print("golden tag test score: {}".format(abs(points_arr_golden[1][1] - points_arr_test[1][1])/pos_golden.shape[1]))
#
# # euclidean distance
# def positioning_score(points_arr_golden, points_arr_test):
#     pos_score_arr = []
#     for idx in range(len(points_arr_test)):
#         pos_score_arr.append(distance.euclidean(points_arr_golden[idx], points_arr_test[idx]))
#         print(pos_score_arr)
#
#     return pos_score_arr
#
# pos_score_arr = positioning_score(points_arr_golden, points_arr_test)
#
#
# plt.imshow(pos_test)
# plt.show()
# cv2.rectangle(img, (x1, y1), (x2, y2), (255,0,0), 2)

#
# # box = cv2.boxPoints(rect)
# # box = np.int0(box)
# # print("bounding box: {}".format(box))
# # # draw the roated rectangle box in the image
# # cv2.drawContours(imgt, [box], 0, (0, 0, 255), 2)
#
# # histogram, bin_edges = np.histogram(im_crop, bins=256, range=(0, 1))
# # np.corr(a,b)
# # plt.plot(histogram)
#
# # plt.imshow(golden_tags_crop_arr[1])
# # plt.show()
